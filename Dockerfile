FROM node:14
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app
ENV NODE_ENV="production"
COPY package.json ./
USER node

RUN npm install
COPY --chown=node:node . .
CMD node src/server.js
EXPOSE 11443

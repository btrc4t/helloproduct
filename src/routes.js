const express = require('express');
const events = require('events');
const router = express.Router();
const { validationChecks, 
  photoViewerLimiter, formSubmitLimiter, 
  auth, normalizeFileRowsErrors } = require('./helpers');
const { Dao, EventResult } = require('./dao');
const { validationResult, matchedData } = require('express-validator');
const csrf = require('csurf');
const fs = require('fs');

const dao = new Dao();

//API to fetch images: /viewRec?id=1&hash=52e6d0cfd49abec9cfb70d4bee911be0
router.get("/viewRec", photoViewerLimiter, auth, async function (req, res) {
    if (!req.query.id || isNaN(req.query.id) || !req.query.hash || req.query.hash.length != 32) 
      return res.end("Invalid input(s)");
      
    const result = await dao.getImagesById(req.query.id);
    if (!result || result.length === 0 || typeof result === 'undefined') 
      return res.end("No result found in database");

    var found = false;
    var resultKeys = Object.keys(result[0]);
    for(var i = 0; i < resultKeys.length; i++) {
      if (found) {
        console.log('showing image', result[0][resultKeys[i]]);
        fs.readFile(result[0][resultKeys[i]], function (err, content) {
            if (err) {
                res.writeHead(400, {'Content-type':'text/html'})
                console.log(err);
                return res.end("No such image");    
            } else {
                //specify the content type in the response will be an image
                res.writeHead(200,{'Content-type':'image/jpg'});
                return res.end(content);
            }
          });
        break;
      }
      if (resultKeys[i].includes("Hash") // current col photoHashX
        && result[0][resultKeys[i]] === req.query.hash) {
        found = true; // executes the if (found) next iteration with col photoLocationX
      }
    }

    if (!found) return res.end("Did not find an image with that hash");
});

const csrfProtection = csrf({ cookie: true });

router.get('/', csrfProtection, (req, res) => {
  res.render('index', {
    data: {},
    errors: {},
    csrfToken: req.csrfToken()
  });
});

router.post('/', formSubmitLimiter, dao.upload.fields([{name: 'photo'}, {name: 'photo2'}, {name: 'photo3'}]),
validationChecks, csrfProtection,  
async function(req, res) {
  const errors = validationResult(req);
  const data = matchedData(req);
  if (!errors.isEmpty()) {
    normalizedErrors = normalizeFileRowsErrors(errors.mapped());
    return res.render('index', {
      data: req.body,
      errors: normalizedErrors,
      csrfToken: req.csrfToken()
    });
  } else {
    dao.formEventEmitter.emit(req.body['_csrf'], data, Object.keys(req.files).length, 
    function (result) {
      console.log('render on result: ',result);
      renderData = {
        formEntryResult: (result === EventResult.FORM_SUCCESS),
        formEntryResultMsg: result,
        data: {},
        errors: {},
        csrfToken: req.csrfToken()
      }
      if (result != EventResult.FORM_SUCCESS) {
        renderData.data = req.body;
        renderData.errors = errors.mapped();
      }      
      return res.render('index', renderData);
    });
  } 
});

module.exports = router;

const { check, oneOf } = require('express-validator');
const rateLimit = require("express-rate-limit");
const basicAuth = require('basic-auth');
const config = require('config');
const fs = require('fs');
const path = require('path');

const auth = function (req, res, next) {
  var user = basicAuth(req);
  if (!user || !user.name || !user.pass) {
    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    res.sendStatus(401);
    return;
  }
  if (user.name === config.get('adminInterface.user')
   && user.pass === config.get('adminInterface.pass')) {
    next();
  } else {
    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    res.sendStatus(401);
    return;
  }
}

function getDbConfig() {
  try {
    dbCfg = config.get('dbConfig');
    var productionEnv = (process.env.NODE_ENV === "production");

    const databaseConfig = {
      connectionLimit: dbCfg.connectionLimit,
      host: dbCfg.host,
      database: productionEnv ? fs.readFileSync(path.resolve(dbCfg.database)).toString().replace('\n', '') 
        : dbCfg.database,
      user: productionEnv ? fs.readFileSync(path.resolve(dbCfg.user)).toString().replace('\n', '') 
        : dbCfg.user,
      password: productionEnv ? fs.readFileSync(path.resolve(dbCfg.password)).toString().replace('\n', '') 
        : dbCfg.password,
    };
    return databaseConfig;
  } catch (e) {
    console.error('getDbConfig', e);
  }
  return null;
}

function getTlsConfig() {
  try {
    tlsCfg = config.get('tls');
    const tlsConfig = {
      key: fs.readFileSync(path.resolve(tlsCfg.key)),
      cert: fs.readFileSync(path.resolve(tlsCfg.cert)),
      dhparam: fs.readFileSync(path.resolve(tlsCfg.dhparam)),
    };
    return tlsConfig;
  } catch (e) {
    console.error('getTlsConfig', e);
  }
  return null;
}

function getSessionConfig() {
  try {
    sCfg = config.get('session');
    var productionEnv = (process.env.NODE_ENV === "production");

    const sessionConfig = {
      secret: productionEnv ? fs.readFileSync(path.resolve(sCfg.secret)).toString().replace('\n', '') 
        : sCfg.secret,
      key: productionEnv ? fs.readFileSync(path.resolve(sCfg.key)).toString().replace('\n', '') 
        : sCfg.key,
    };
    return sessionConfig;
  } catch (e) {
    console.error('getSessionConfig', e);
  }
  return null;
}

const photoViewerLimiter = rateLimit({
  windowMs: 10 * 60 * 1000, // 10 minutes window
  max: 100 // blocking on more than 100 requests
});

const formSubmitLimiter = rateLimit({
  windowMs: 30 * 60 * 1000, // 30 minutes window
  max: 50, // blocking on more than 60 requests
});

const receiptRowChecker = (value, { req, path }) => {
  if (Object.keys(req.files).includes(path)) { //photo
    return true;
  } else if (value === '' || value === 'undefined') { //other
    throw new Error("Please fill this field");
  } else if (path.includes('Number') || path.includes('Value')) {
    if (value.split(",").length - 1 > 1 || value.split(".").length - 1 > 1 
    || (value.includes('.') && value.includes(','))) 
      throw new Error("Please use only one '.' or ',' character as a decimal separator");
    parsedValue = parseFloat(value.replace(',','.').replace(' ',''));
    if (isNaN(parsedValue)) throw new Error("The value entered is not a number");
    return true;
  } else if (path.includes('Date')) {
    let date = new Date(value);
    if (!(date instanceof Date) && isNaN(date)) throw new Error("Please enter a valid date");
    return true;
  } else {
    throw new Error("Upload an image of the receipt");
  }
}

const validationChecks = [
  check('name')
    .isLength({ min: 2 })
    .withMessage('Name is required')
    .trim(),
  check('email')
    .isEmail()
    .withMessage('Email address is required')
    .bail()
    .normalizeEmail()
    .trim(),
  check('number')
    .isMobilePhone()
    .withMessage('Phone number is required')
    .trim(),
  check('address')
    .isLength({ min: 2 })
    .withMessage('Address is required')
    .trim(),  
  oneOf([
    check(['receiptDate', 'receiptNumber', 'receiptValue', 'photo']).custom(receiptRowChecker),
    check(['receiptDate2', 'receiptNumber2', 'receiptValue2', 'photo2']).custom(receiptRowChecker),
    check(['receiptDate3', 'receiptNumber3', 'receiptValue3', 'photo3']).custom(receiptRowChecker),
  ], 'At least one receipt row should be filled completely'),
  check('consent')
    .exists()
    .withMessage('You must consent to have your data processed')
];

function indexOfSmallest(a) {
  var lowest = 0;
  for (var i = 1; i < a.length; i++) {
    if (a[i] < a[lowest]) lowest = i;
  }
  return lowest;
}

function normalizeFileRowsErrors(errors) {
  if (typeof errors['_error'] === 'undefined' || typeof errors['_error']['nestedErrors'] === 'undefined') {
    console.log('returned plain errors, no errors for file rows');
    return errors;
  }

  var toReturn = {};
  var allRowsErrors = [{}, {}, {}];
  var rowErrors = [0, 0, 0];
  Object.assign(toReturn, errors);
  
  // Move nested errors of fileRowsErrors to the top level in allRowsErrors
  var fileRowsErrors = errors['_error']['nestedErrors'];
  for (var i in fileRowsErrors) {
    // count only unfilled fields
    var countAsError = fileRowsErrors[i]['value'] === '' || typeof fileRowsErrors[i]['value'] === 'undefined';
    if (fileRowsErrors[i]['param'].includes("2")) { //receiptDate2, photo2, etc..
      allRowsErrors[1][fileRowsErrors[i]['param']] = fileRowsErrors[i];
      if (countAsError) rowErrors[1]++;
    } else if (fileRowsErrors[i]['param'].includes("3")) { //receiptDate3, photo3, etc..
      allRowsErrors[2][fileRowsErrors[i]['param']] = fileRowsErrors[i];
      if (countAsError) rowErrors[2]++;
    } else { //receiptDate, photo, etc..
      allRowsErrors[0][fileRowsErrors[i]['param']] = fileRowsErrors[i];
      if (countAsError) rowErrors[0]++;
    }
  }

  // Keep only the errors for the row where the smallest # of errors exist
  // or for the row where # of errors is under 4
  let smallestErrorCountRowIndex = indexOfSmallest(rowErrors);
  for (var i in allRowsErrors) {
    if (i == smallestErrorCountRowIndex || rowErrors[i] < 4) { 
      Object.assign(toReturn, allRowsErrors[i]);
    }
  }

  delete toReturn['_error'];
  return toReturn;
}

module.exports = {
  validationChecks,
  photoViewerLimiter,
  formSubmitLimiter,
  auth,
  normalizeFileRowsErrors,
  getDbConfig,
  getTlsConfig,
  getSessionConfig
}
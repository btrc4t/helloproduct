const events = require('events');
const mysql = require('mysql');
const fs = require('fs');
const sharp = require('sharp');
const md5 = require('md5');
const multer = require('multer');
const { getDbConfig } = require('./helpers');

EventResult = {
  FORM_SUCCESS: 'Complete success',
  FORM_FAIL_INSERT: 'Internal insert error',
  FORM_SUCCESS_INSERT: 'Internal insert success',
  DB_FAIL_CONN: 'Internal database connection failed',
  DB_SUCCESS_CONN: 'Internal database connection success',
  STORAGE_FAIL_PHOTO: 'Internal storage generic error',
  STORAGE_FAIL_PHOTO_HASH: 'Uploaded photo is not unique',
  STORAGE_FAIL_PHOTO_STORE: 'Internal storage write error',
  STORAGE_NOT_DONE: 'Internal storage processing error',
  STORAGE_SUCCESS_PHOTO: 'Internal storage success',
}

class HashObject {
  constructor(hashArray, photoArray, bufferArray, invalidArray) {
      this.hashArray = hashArray;
      this.photoArray = photoArray;
      this.bufferArray = bufferArray;
      this.invalidArray = invalidArray;
  }
}

class MyCustomStorage {
  constructor(dao) {
    this.dao = dao;
  }

  getDestination = function (req, file, cb) { 
    return cb(null, 'data/images/' + Date.now() + '_' + file.originalname)
  }

  _handleFile (req, file, cb) {
    var daoRef = this.dao;
    this.getDestination(req, file, async function (err, path) {
      // console.log('storage csrf', req.body['_csrf']);
      if (err) return cb(err);
      var chunks = [];
      file.stream.on('data', (chunk) => {
          chunks.push(chunk); // push data chunk to array
      });
  
      file.stream.once('end', async () => {
          // create the final data Buffer from data chunks;
          const fileBuffer = Buffer.concat(chunks);
  
          // Wait for the form to emit the same event (only when the form is valid)
          daoRef.formEventEmitter.once(req.body['_csrf'], async function(data, nrOfFiles, whenDone) {
              var imgStorageResult = await daoRef.saveImageAndData(daoRef.db, fileBuffer, path, req.body['_csrf'], data, nrOfFiles);
              console.log('storage done with result: ',imgStorageResult);
              if (imgStorageResult != EventResult.STORAGE_NOT_DONE) 
                  whenDone(imgStorageResult);
          });
  
          setTimeout(() => {
            daoRef.formEventEmitter.removeAllListeners(req.body['_csrf']);
          }, 60000); // After 60s of the event not being emitted, remove listener
          
          cb(null, {
              path: path,
              size: chunks.length
          });
      });  
    });
  }

  _removeFile = function _removeFile (req, file, cb) {
    fs.unlink(file.path, cb)
  }

}

class Dao {
  constructor() {
    // Make directories if inexistent
    this.dir = './data/images';
    !fs.existsSync(this.dir) && fs.mkdirSync(this.dir, {recursive: true});
    this.db = this.initDatabase();
    this.storage = new MyCustomStorage(this);

    this.imageFilter = function(req, file, cb) {
      // Accept images only
      if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
          req.fileValidationError = 'Please select an image file!';
          return cb(null, false);
      }
      cb(null, true);
    };

    this.upload = multer({ 
      storage: this.storage, 
      fileFilter: this.imageFilter,
      limits: {
        fileSize: 12 * 1024 * 1024 // 12MB
      },
    });
    
  }

  hashObjectsDict = {};

  formEventEmitter = new events.EventEmitter;

  insert = 'INSERT INTO register (name, email, number, address, ' +
    'receiptDate, receiptNumber, receiptValue, ' +
    'receiptDate2, receiptNumber2, receiptValue2, ' +
    'receiptDate3, receiptNumber3, receiptValue3, ' +
    'photoHash, photoLocation, photoHash2, photoLocation2, ' +
    'photoHash3, photoLocation3, entryDate)' +
    ' VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);';

  checkEmailExists = 'SELECT `email`, ' +
                      '`name` ' +
                      'FROM `register` ' +
                      'WHERE `email` = ?';

  getPhotos = 'SELECT `photoHash`, `photoLocation`, ' +
                      '`photoHash2`, `photoLocation2`, ' +
                      '`photoHash3`, `photoLocation3` ' +
                      'FROM `register` ' +
                      'WHERE `id` = ?';

  getHashId = 'SELECT `id` ' +
                    'FROM `register` ' +
                    'WHERE `photoHash` = ? OR ' +
                    '`photoHash2` = ? OR ' +
                    '`photoHash3` = ?';
    
  createTable = 'CREATE TABLE IF NOT EXISTS register ( ' +
    'id BIGINT(63) UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE, ' +
    'name VARCHAR(45), ' +
    'email VARCHAR(45), ' +
    'number VARCHAR(12), ' +
    'address VARCHAR(64), ' +
    'receiptDate DATE, ' +
    'receiptNumber INT, ' +
    'receiptValue DOUBLE, ' +
    'receiptDate2 DATE, ' +
    'receiptNumber2 INT, ' +
    'receiptValue2 DOUBLE, ' +
    'receiptDate3 DATE, ' +
    'receiptNumber3 INT, ' +
    'receiptValue3 DOUBLE, ' +
    'photoHash VARCHAR(32), ' +
    'photoLocation VARCHAR(64), ' +
    'photoHash2 VARCHAR(32), ' +
    'photoLocation2 VARCHAR(64), ' +
    'photoHash3 VARCHAR(32), ' +
    'photoLocation3 VARCHAR(64), ' +
    'entryDate DATETIME, ' +
    'PRIMARY KEY (id));';

  async getConnectionAndQuery(db, sql, data, cb) {
    return new Promise(resolve => {
      db.getConnection(function(err, connection) {
        if (err) {
          console.error('getConnectionAndQuery:', err.message);
          resolve(EventResult.DB_FAIL_CONN);
          process.exit(42);//db
        }
        connection.beginTransaction();
        connection.query(sql, data, cb);
        db.releaseConnection(connection);
        resolve(EventResult.DB_SUCCESS_CONN);
        });
    });
  }

  initDatabase() {
    console.log('init database');
    // check if database file exists, if not create it
    console.log('environment is', process.env.NODE_ENV);

    var dbConfig = getDbConfig();
    var db = mysql.createPool(dbConfig);

    this.getConnectionAndQuery(db, this.createTable, [], function (error, results, fields) {
      console.log('called from initDatabase()');
      if (error) {
        console.log("error is inside init");
        console.error(error.message);
        throw error;
      }
      console.log('Register table exists.');
    }).then(function (result) {
      console.log('initDatabase result',result);
    });

    return db;
  }

  getImagesById(id) {
    return new Promise(resolve => {
      this.getConnectionAndQuery(this.db, this.getPhotos, id, 
      function (err, result, fields) {
        if (err || !result) {
          resolve(null);
        }
        resolve(result);
      }).then((_) => resolve(null));
    });
  }

  saveImageAndData(db, fileBuffer, path, csrf, data, nrOfFiles) {
    var context = this;
    return new Promise(resolve => {
        sharp(fileBuffer)
        .resize(800, 800, {
            fit: sharp.fit.inside,
            withoutEnlargement: true
        })
        .jpeg({
            quality: 80,
        }).toBuffer().then(async function (buffer) {
            let hash = md5(buffer);
            var hashFound = await context.findHash(hash);
            if (hashFound === EventResult.DB_FAIL_CONN) {
              resolve(EventResult.DB_FAIL_CONN);
              return;
            }
            console.log('findHash Result', hashFound);
            var invalid = (hashFound.length > 0);
            let newPath = "data/images/" + Date.now() + '_'  + hash + '.jpg';
            if (context.hashObjectsDict[csrf]) {
                context.hashObjectsDict[csrf].photoArray.push(newPath); 
                context.hashObjectsDict[csrf].hashArray.push(hash);    
                context.hashObjectsDict[csrf].bufferArray.push(buffer); 
                context.hashObjectsDict[csrf].invalidArray.push(invalid);                
            } else {
                context.hashObjectsDict[csrf] = new HashObject([hash],[newPath],[buffer],[invalid]);
            }

            if (nrOfFiles > 0 && nrOfFiles === context.hashObjectsDict[csrf].photoArray.length) {
                if (context.hashObjectsDict[csrf].invalidArray.includes(true) ) {
                    resolve(EventResult.STORAGE_FAIL_PHOTO_HASH);
                    delete context.hashObjectsDict[csrf];
                    return;
                }
                var formEntryResult = await context.formEntry(data, context.hashObjectsDict, csrf);
                console.log('formEntry resolved: ',formEntryResult);
                
                switch(formEntryResult) {
                    default:
                        resolve(EventResult.STORAGE_FAIL_PHOTO);
                        break;
                    case EventResult.FORM_FAIL_INSERT:
                        resolve(formEntryResult);
                        break;
                    case EventResult.FORM_SUCCESS_INSERT:
                        for(var i = 0; i < context.hashObjectsDict[csrf].photoArray.length; i++) {
                            fs.writeFile(context.hashObjectsDict[csrf].photoArray[i], context.hashObjectsDict[csrf].bufferArray[i], function(err) {
                                if (err) {
                                    console.log('storage err:',err);
                                    resolve(EventResult.STORAGE_FAIL_PHOTO_STORE);
                                }
                            });
                        }
                        resolve(EventResult.FORM_SUCCESS);
                        break;
                }
                delete context.hashObjectsDict[csrf];
            } else {
                resolve(EventResult.STORAGE_NOT_DONE);
            }
        });
    });    
  }

  findHash(hash) {
    var context = this;
    return new Promise(resolve => {
      context.getConnectionAndQuery(context.db, context.getHashId, [hash, hash, hash], 
      function (err, result, fields) {
        if (err || !result) {
          resolve(err.sqlMessage);
        }
        resolve(result);
      });
    });
  }

  async formEntry(data, hashObj, key) {
    var context = this;
    return new Promise(resolve => {
      console.log(`form-entry fired with csrf:`, key);    
      this.getConnectionAndQuery(this.db, this.insert, 
        [data['name'], data['email'], data['number'], data['address'],
        data['receiptDate'], data['receiptNumber'], data['receiptValue'],
        data['receiptDate2'], data['receiptNumber2'], data['receiptValue2'],
        data['receiptDate3'], data['receiptNumber3'], data['receiptValue3'],
        hashObj[key]['hashArray'][0], hashObj[key]['photoArray'][0], 
        hashObj[key]['hashArray'][1], hashObj[key]['photoArray'][1], 
        hashObj[key]['hashArray'][2], hashObj[key]['photoArray'][2], new Date()
      ], 
      async function (err, res, fields) {
        const result = await context.checkDb(data['email']);
        
        console.log('result from checkDb: ',result);
        if (err || res.affectedRows < 1 || !result) {
          console.error('form-entry insert error: ',err ? err.sqlMessage : 'affectedRows zero / result negative');
          resolve(EventResult.FORM_FAIL_INSERT);
        }
        resolve(EventResult.FORM_SUCCESS_INSERT);
      });
    });  
  }

  checkDb(email) {
    console.log("checking database entries")
    var context = this;
    return new Promise(resolve => {
      this.getConnectionAndQuery(context.db, context.checkEmailExists, [email], 
        function (err, res, fields) {
          if (err) {
            console.error(err.message);
            resolve({regs: fields,
                      err});
          }
          
          console.log(res.length  + ' entries found')
          resolve(res.length > 0);
        }
      );
    });
  }
}

module.exports = { Dao, EventResult };
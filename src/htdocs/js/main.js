function moveToSelected(element) {
    if (element == "next") {
        var selected = $(".selected").next();
    } else if (element == "prev") {
        var selected = $(".selected").prev();
    } else {
        var selected = element;
    }
    
    var next = $(selected).next();
    var prev = $(selected).prev();
    var prevSecond = $(prev).prev();
    var nextSecond = $(next).next();

    $(selected).removeClass().addClass("selected");

    $(prev).removeClass().addClass("prev");
    $(next).removeClass().addClass("next");

    $(nextSecond).removeClass().addClass("nextRightSecond");
    $(prevSecond).removeClass().addClass("prevLeftSecond");

    $(nextSecond).nextAll().removeClass().addClass('hideRight');
    $(prevSecond).prevAll().removeClass().addClass('hideLeft');

}

$(document).on("keydown", function(e) {
    switch(e.keyCode) {
        case 37: // left
        moveToSelected('prev');
        break;

        case 39: // right
        moveToSelected('next');
        break;

        default: return;
    }
    e.preventDefault();
});

$(window).on("load", function() {
    $('#carousel-control-prev').on("click", function() {
        moveToSelected('prev');
    });
    $("#carousel-control-next").on("click", function() {
        moveToSelected("next");
    });
    if ($(".is-invalid").length > 0 || $(".toast").length > 0) {
        document.getElementById("name").scrollIntoView({behavior: 'auto'});
    }
    // Enable BS tooltips
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
    })
    // Show BS toast
    if ($(".toast").length > 0) {
        $('.toast').toast('show');
    }

});




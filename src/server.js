const path = require('path');
const express = require('express');
const layout = require('express-layout');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const MySqlStore = require('express-mysql-session')(session);
const flash = require('express-flash');
const helmet = require('helmet');
const routes = require('./routes');
const dateFormat = require("dateformat");
const https = require('https')
const { getDbConfig, getTlsConfig, getSessionConfig } = require('./helpers');

dbConfig = getDbConfig();

tlsConfig = getTlsConfig();

sessionConfig = getSessionConfig();

var sessionStore = new MySqlStore({
  connectionLimit: dbConfig.connectionLimit,
  host: dbConfig.host,
	port: 3306,
	user: dbConfig.user,
	password: dbConfig.password,
	database: dbConfig.database,
  createDatabaseTable: true,
});

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
const middlewares = [
  helmet(),
  layout(),
  express.static(path.join(__dirname, 'htdocs')),
  express.urlencoded({ extended: true }),
  cookieParser(),
  session({
    secret: sessionConfig.secret,
    key: sessionConfig.key,
    store: sessionStore,
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 60000 }
  }),
  flash(),
];
app.use(middlewares);

app.use('/', routes);

app.use((req, res, next) => {
  res.status(404).send("Sorry can't find that!");
});

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

app.locals.dateFormat = dateFormat;

https.createServer(tlsConfig, app).listen(11443, function () {
  console.log('App running at https://localhost:11443');
});

# Installation Steps

1. Clone repo
2. Install dependencies
```bash
npm install
```
3. Set up a MySQL / MariaDB database locally
4. Check **config/*.example**, rename contained files to have the **config/*.json** extension and adjust parameters
5. Check **config/custom-environment-variables.json.example** to view the environment variables to use to accomodate your environment
6. Deploy development application version and set your environment variables
```bash
ADM_USER=user ADM_PASS=password DB_PASS=sqlPass [..] nodejs src/server.js 
``` 
7. Use the Docker commands below to deploy the live application version
8. Visit localhost:11443 for the development version or localhost:443 for the live app

# Development pointers
1. Automatically compile modified scss files 
```bash
npm run scss
```

# Docker deployment steps

## Deployment:
1. Create a swarm with one machine, if needed
```bash
docker swarm init 
```
2. Create secrets: 
```bash
echo "dbRootPass" | docker secret create db_root_pass -
echo "dbName" | docker secret create db_name -
echo "dbAdminUser" | docker secret create db_admin_user -
echo "dbAdminPass" | docker secret create db_admin_pass -
echo "appAdminUser" | docker secret create app_admin_user -
echo "appAdminPass" | docker secret create app_admin_pass -
echo "sessionSecret" | docker secret create session_secret -
echo "sessionKey" | docker secret create session_key -
docker secret create tls_key keys/key.pem
docker secret create tls_cert keys/cert.pem
docker secret create tls_dhparam keys/dh-strong.pem
```
3. Build the docker image for the application
```bash
docker build . -t campaign-app:latest
```
4. Deploy a stack for the application (Use the `formstack` name, it is used to resolve the IP address of the DB service)
```bash
docker stack deploy -c docker-compose.yml formstack
```

## Teardown:
1. Remove stack
```bash
docker stack rm formstack
```
2. Remove image
```bash
docker rmi campaign-app 
```

# TODO
1. Maybe add linter
2. Maybe write tests


